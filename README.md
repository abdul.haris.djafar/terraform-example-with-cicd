# TERRAFORM-EXAMPLE-WITH-CI/CD

## How To Use it ?
    1. Create service account with this roles :
        - Compute Admin
        - Kubernetes Engine Admin
        - Service Account User
    2. Download service account json file
    3. In `GCP_CONFIG_FILE` ci/cd variable fill it with service account json file. the type for GCP_CONFIG_FILE is file
    4. start ci/cd

## Setup terraform.tfvars
    In `TERRAFORM_TFVARS` ci/cd variable fill it with below datas. The type of TERRAFORM_TFVARS is file.

    ```
    provider_project=""
    provider_region="us-central1"
    provider_zone="us-central1-c"
    container_node_pool_name="kotekaman-sexy"
    container_node_pool_node_count=2
    container_cluster_name="kotekaman"
    vpc_name="vpc-xxx"
    subnet_name="subnet-xxx"
    ```

    ```
    provider_project => google project id
    provider_region => region that want to choose
    provider_zone => zone from  region that want to choose
    container_node_pool_name => gke pool node name
    container_node_pool_node_count => node amount
    container_cluster_name => gke cluster name
    vpc_name => vpc name
    subnet_name => subnet name
    ```

