output "gke_cluster_id" {
  description = "gke id created"
  value       = "${google_container_cluster.kotekaman_container.id}"
}