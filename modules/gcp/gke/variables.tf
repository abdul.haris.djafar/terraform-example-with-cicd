variable "container_cluster_name"{
    description = "define cluster name"
    default     = "kotekaman"
} 

 variable "container_cluster_location" {
    description = "define location for the cluster"
    default     = "us-central1"
}

variable "container_cluster_network"{
    description = "define network for the cluster"
    default = ""
}

variable "container_cluster_subnetwork"{
    description = "define cluster subnetwork"
    default       = ""
}

variable "container_cluster_remove_default_node_pool"{
    description = "remove the default node pool or not"
    default = true
}

variable "container_cluster_initial_node_count"{
    description = "define initial amount of node"
    default = 1
}



variable "container_node_pool_node_count"{
    description = "amount of node in pool"
    default = 1
}

variable "container_node_pool_node_count_config_preemptible"{
    description = "define preemptible. is active or not"
    default = true
} 

variable "container_node_pool_node_count_config_machine_type"{
    description = "define machine type in pool"
    default = "e2-medium"
} 
variable "container_node_pool_node_count_config_oauth_scopes"{
    description = "what ouaths scopes will allowed"
    default = [ "https://www.googleapis.com/auth/cloud-platform" ]
}
variable "container_node_pool_name" {
    description = "define node pool name"
    default = "kotekaman-node-pool-1"
}

variable "container_node_pool_location"{
    description = "define node pool location"
    default = "us-central1"
} 

variable "container_node_pool_cluster_name"{
    description = "define node pool location"
    default = "value"
} 


variable "container_node"{
    description = "node initialize name"
    default = "kotekaman-node-1"
}