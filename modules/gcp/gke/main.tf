# create gke cluster
resource "google_container_cluster" "kotekaman_container"{
    name = "${var.container_cluster_name}"
    location = "${var.container_cluster_location}"
    remove_default_node_pool = "${var.container_cluster_remove_default_node_pool}"
    initial_node_count = "${var.container_cluster_initial_node_count}"
    network = "${var.container_cluster_network}"
    subnetwork = "${var.container_cluster_subnetwork}"
}

module "node_pool" {
  source = "./node_pool"

  container_node_pool_name = "${var.container_node_pool_name}"
  container_node_pool_location = "${var.container_node_pool_location}"
  container_node_pool_cluster_name = "${var.container_cluster_name}"
  container_node_pool_node_count = "${var.container_node_pool_node_count}"
  container_node_pool_node_count_config_preemptible = "${var.container_node_pool_node_count_config_preemptible}"
  container_node_pool_node_count_config_machine_type = "${var.container_node_pool_node_count_config_machine_type}"
  container_node_pool_node_count_config_oauth_scopes = "${var.container_node_pool_node_count_config_oauth_scopes}"
  container_node = "${var.container_node}"

  depends_on = [
    google_container_cluster.kotekaman_container
  ]
}

