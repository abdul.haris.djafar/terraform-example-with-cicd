variable "container_node_pool_name" {
    description = "define node pool name"
    default = "kotekaman-node-pool-1"
}

variable "container_node_pool_location"{
    description = "define node pool location"
    default = "us-central1"
} 


variable "container_node_pool_cluster_name"{
    description = "define node pool location"
    default = "kotekaman1"
} 

variable "container_node_pool_node_count"{
    description = "amount of node in pool"
    default = 1
}

variable "container_node_pool_node_count_config_preemptible"{
    description = "define preemptible. is active or not"
    default = true
} 

variable "container_node_pool_node_count_config_machine_type"{
    description = "define machine type in pool"
    default = "e2-medium"
} 
variable "container_node_pool_node_count_config_oauth_scopes"{
    description = "what ouaths scopes will allowed"
    default = [ "https://www.googleapis.com/auth/cloud-platform" ]
}

variable "container_node"{
    description = "node initialize name"
    default = "kotekaman-node-1"
}

variable "provider_project" {
    description = "project id"
    default = "bei-infra-testing"
}

variable "provider_region"{
    description = "region for create instance"
    default = "us-central1"
}  
variable "provider_zone"{
    description = "zone for create instance"
    default = "us-central1-c"
}    