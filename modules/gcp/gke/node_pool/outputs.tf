output "node_pool" {
  description = "node pool"
  value       = "${google_container_node_pool.kotekaman_pool}"
}

output "node_pool_id" {
  description = "node pool id"
  value       = "${google_container_node_pool.kotekaman_pool.id}"
}