# create node pool
resource "google_container_node_pool" "kotekaman_pool" {
  name = "${var.container_node_pool_name}"
  location = "${var.container_node_pool_location}"
  cluster = "${var.container_node_pool_cluster_name}"
  node_count = "${var.container_node_pool_node_count}"

  node_config {
    preemptible = "${var.container_node_pool_node_count_config_preemptible}"
    machine_type = "${var.container_node_pool_node_count_config_machine_type}"
    oauth_scopes = "${var.container_node_pool_node_count_config_oauth_scopes}"
  }
}