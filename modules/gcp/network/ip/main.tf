resource "google_compute_address" "internal_address" {
  subnetwork   = "${var.internal_address_subnetwork}"
  name         = "${var.internal_address_name}-internal-address"
  address      = "${var.ip}"
  address_type = "${var.internal_address_type}"
}