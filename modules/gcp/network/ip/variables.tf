variable "internal_address_name" {
  description = "internal adress name"
  default     = "new-vpc-internall-address"
}

variable "internal_address_type" {
  description = "internal address type"
  default = "INTERNAL"
}

variable "internal_address_subnetwork" {
  description = "internal subnetwork"
  default = ""
}

variable "ip" {
  description = "ip for addressing"
  default = ""
}