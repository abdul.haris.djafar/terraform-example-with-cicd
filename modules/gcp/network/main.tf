resource "google_compute_subnetwork" "network-with-private-secondary-ip-ranges" {
  name          = "${var.subnet_name}"
  ip_cidr_range = "${var.subnet_ip_cidr_range}"
  region        = "${var.subnet_region}"
  private_ip_google_access = true
  network       = google_compute_network.custom.id
}

resource "google_compute_network" "custom" {
  name                    = "${var.vpc_name}"
  auto_create_subnetworks = false
}