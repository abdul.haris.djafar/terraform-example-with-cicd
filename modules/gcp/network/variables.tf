variable "vpc_name" {
    description = "vpc name for environmetn"
    default = "new-vpc"
}

variable "subnet_name"  {
    description = "subnet name"
    default = "test-subnetwork"
}        
variable "subnet_ip_cidr_range"{
    description = "ip cidr for set up the subnet"
    default = "10.2.0.0/16"
} 
variable "subnet_region"{
    description = "region for subnet"
    default        = "us-central1"
}