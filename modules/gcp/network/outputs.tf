output "vpc_self_link" {
  value = google_compute_network.custom.self_link
}

output "subnet_self_link" {
  value = google_compute_subnetwork.network-with-private-secondary-ip-ranges.self_link
}

output "vpc_id" {
  value = google_compute_network.custom.id
}

output "subnet_id" {
  value = google_compute_subnetwork.network-with-private-secondary-ip-ranges.id
}