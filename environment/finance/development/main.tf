provider "google" {
  project = "${var.provider_project}"
  region  = "${var.provider_region}"
  credentials = "${file("../../../gcp_config.json")}"
  
}

module "network"{
  source = "../../../modules/gcp/network"
  vpc_name = "${var.vpc_name}"
  subnet_name = "${var.subnet_name}"
  
}

module "custom_gke" {
  source ="../../../modules/gcp/gke"
  container_cluster_network = module.network.vpc_self_link
  container_cluster_subnetwork = module.network.subnet_self_link
  container_cluster_location = "${var.provider_zone}"
  container_cluster_name = "${var.container_cluster_name}"
  container_node_pool_node_count = "${var.container_node_pool_node_count}"
  container_node_pool_location = "${var.provider_zone}"
  container_node_pool_name = "${var.container_node_pool_name}"
  
  depends_on = [
    module.network
  ]
}
