variable "provider_project" {
    description = "project id"
    default = "bei-infra-testing"
}

variable "provider_region"{
    description = "region for create instance"
    default = "us-central1"
}  

variable "provider_zone"{
    description = "zone for create instance"
    default = "us-central1-c"
}    



variable "container_node_pool_name" {
    description = "define node pool name"
    default = "kotekaman-node-pool-1"
}


variable "container_node_pool_node_count"{
    description = "amount of node in pool"
    default = 2
}

variable "container_cluster_name"{
    description = "define cluster name"
    default     = "new-kotekaman"
} 

 variable "container_cluster_location" {
    description = "define location for the cluster"
    default     = "us-central1-c"
}

variable "vpc_name" {
    description = "vpc name for environmetn"
    default = "new-vpc"
}

variable "subnet_name"  {
    description = "subnet name"
    default = "test-subnetwork"
}       